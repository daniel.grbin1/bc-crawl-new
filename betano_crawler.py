from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from concurrent.futures import ThreadPoolExecutor
import urllib3
import json
from bs4 import BeautifulSoup
import datetime
import time

url_localhost = "http://localhost:3000"
user_url = url_localhost

http = urllib3.PoolManager(cert_reqs='CERT_NONE')
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def selenium_init():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    # Setting big window height in order to load all the matches at once
    chrome_options.add_argument("window-size=1920,10080")
    driver = webdriver.Chrome(options=chrome_options)

    driver.get("https://www.betano.cz/live/")
    time.sleep(2)  # Explicit Selenium wait would also be possible to use here
    try:
        driver.find_element(By.CLASS_NAME, "sb-modal__close__btn").click()
    except:
        pass

    return driver


def fetch(url):
    page_cr = http.request('GET', url)
    return page_cr.data


def prematch():
    # URLs in predefined list, in order to improve speed and avoid repeated league ids scraping
    urls = [
        'https://www.betano.cz/sport/fotbal/liga/16905r,16876r,187416r,183634r,185364r,192225r,181988r,16894r,183982r,16909r,183707r,187579r,181769r,16932r,17561r,17600r,17249r,18165r,198653r,16940r',
        'https://www.betano.cz/sport/fotbal/liga/16952r,17427r,17460r,182748r,182761r,196755r,1r,218r,10215r,2r,527r,4r,10767r,1697r,17877r,1698r,18092r,18443r,192991r,192992r',
        'https://www.betano.cz/sport/fotbal/liga/10392r,17497r,17088r,17087r,17524r,17073r,183362r,193966r,192558r,193135r,182010r,1647r,10345r,1630r,1672r,1673r,183456r,183592r,184030r,17315r',
        'https://www.betano.cz/sport/fotbal/liga/187081r,187082r,181923r,192009r,16816r,189802r,183683r,197698r,17988r,17318r,17360r,17458r,17543r,18206r,181734r,182147r,183604r,183992r,184599r,186782r',
        'https://www.betano.cz/sport/fotbal/liga/196300r,17620r,17823r,187179r,189365r,187273r,17906r,187051r,187259r,192170r,181843r,17412r,184866r,17545r,182985r,17049r,17839r,17796r,17729r,17058r',
        'https://www.betano.cz/sport/fotbal/liga/17405r,181614r,17445r,190985r,5r,10067r,10000r,197049r,12227r,17592r,182838r,1635r,10815r,10210r,17714r,15285r,17457r,188767r,195757r,17768r',
        'https://www.betano.cz/sport/fotbal/liga/216r,10486r,217r,17313r,11969r,11971r,17978r,17993r,18492r,184517r,13296r,17459r,186913r,184742r,184740r,15742r,184743r,184741r,11973r,184765r',
        'https://www.betano.cz/sport/fotbal/liga/200268r,17407r,195785r,17244r,17532r,17548r,187941r,18274r,10016r,10008r,16880r,16901r,17910r,18137r,16893r,16887r,16884r,185809r,16888r,16882r',
        'https://www.betano.cz/sport/fotbal/liga/17177r,17059r,18112r,17727r,18093r,193291r,17060r,18233r,194900r,181982r,185531r,17539r,183628r,181647r,17699r,17827r,191595r,193800r,17853r,181762r',
        'https://www.betano.cz/sport/fotbal/liga/191718r,196161r,194293r,183424r,183037r,194179r,17766r,17239r,17530r,183741r,17578r,184764r,17377r,18442r,187234r,189281r,182106r,186111r,196213r,200011r',
        'https://www.betano.cz/sport/fotbal/liga/17500r,187544r,17572r,181553r,17264r,195435r,184596r,197789r,17383r,17595r,188482r,183321r,17917r,196214r,17078r,17079r,17788r,17103r,17105r,17501r',
        'https://www.betano.cz/sport/fotbal/liga/17370r,17068r,17093r,17091r,17439r,16765r,17381r,17538r,16849r,16842r,17158r,186744r,187295r,184591r,17717r,16918r,16921r,182182r,16941r,16942r',
        'https://www.betano.cz/sport/fotbal/liga/17113r,182210r,17670r,17695r,183792r,17108r,182164r,17118r,17119r,17122r,17124r,17123r,181772r,187550r,185946r,17100r,17403r,187487r,182156r,200263r',
        'https://www.betano.cz/sport/fotbal/liga/17391r,17494r,17819r,187488r,16947r,16946r,17799r,17024r,18198r,17559r,17080r,17081r,17396r,18006r,182828r,182296r,16823r,16826r,18079r,1636r',
        'https://www.betano.cz/sport/fotbal/liga/17737r,183670r,18100r,16955r,16954r,17126r,18270r,185516r,198492r,198502r,183071r,17674r,17041r,18080r,184021r,17836r,17938r,17957r,17491r,17802r',
        'https://www.betano.cz/sport/fotbal/liga/17542r,181613r,185679r,199723r,215r,11963r,10467r,11962r,17129r,190021r,13292r,17083r,17086r,17385r,197549r,17697r,194850r,198200r,17067r,17069r'
    ]

    matches = []
    # urls = get_urls() # this function gets current league ids, needs to be uncommented in order get the most recent league ids
    with ThreadPoolExecutor() as pool:
        for page_content in pool.map(fetch, urls):
            soup = BeautifulSoup(page_content, 'lxml')
            # Desired content can be found within the script tag
            data = soup.find("body").find("script").text.split("=", 1)[1]
            data_json = json.loads(data)
            leagues = data_json["data"]["blocks"]
            for league in leagues:
                league_name = league["name"].replace("\'", "")

                events = league["events"]
                for match in events:
                    date_raw = datetime.datetime.fromtimestamp(
                        match["startTime"] / 1000.0)
                    start_date = date_raw.strftime("%Y-%m-%dT%H:%M:%S")
                    teams = match["name"].split(" - ")
                    if len(teams) != 2:
                        continue

                    team1 = teams[0].replace("\'", "")
                    team2 = teams[1].replace("\'", "")
                    try:
                        odds = match["markets"][0]["selections"]
                        odd_home = odds[0]["price"]
                        odd_draw = odds[1]["price"]
                        odd_away = odds[2]["price"]
                    except IndexError:
                        # It can happen that odds area contains less than 3 fields,
                        # in that case it is some unwanted odds type
                        continue

                    match_final = {'live': False,
                                   'date': start_date,
                                   'league': league_name,
                                   'home': team1,
                                   'away': team2,
                                   'b_1': float(odd_home),
                                   'b_x': float(odd_draw),
                                   'b_2': float(odd_away)
                                   }

                    matches.append(match_final)

    return matches


def live(driver):
    matches = []
    # Passing driver page source to external parsing tool in order to improve scraping speed
    soup = BeautifulSoup(driver.page_source, "lxml")
    blocks = soup.find_all("div", class_="vue-recycle-scroller__item-view")
    league_name = ""
    for block in blocks:
        league_name_list = block.find_all("div", {
            'sport-id': True})  # Finding all div elements with a specific attribute (sport-id)

        if len(league_name_list) != 0:
            # Filtering out non-football matches
            if league_name_list[0]["sport-id"] != "FOOT":
                league_name = None
                continue

            league_name = league_name_list[0].find("span").find(
                "span").get_text()

        if league_name is None:
            continue

        minute_elem = block.find("div", attrs={
            "data-qa": "live-event-time"})  # Finding all div elements with a specific attribute equal to some value
        if minute_elem is None:
            continue

        try:
            minute = minute_elem.find(
                "span").get_text().strip()

            inner_block = block.find("a",
                                     attrs={"data-qa": "live-event"}).find(
                "div", attrs={"data-qa": "participants"})
            teams = inner_block.find_all("div", class_="tw-truncate")

            team1 = teams[0].get_text().strip()
            team2 = teams[1].get_text().strip()

            odds = block.find_all("span", class_="selections__selection__odd")

            if len(odds) != 3:
                continue

            b_1 = odds[0].text.strip() if odds[0].text.strip() != "" else 0
            b_x = odds[1].text.strip() if odds[1].text.strip() != "" else 0
            b_2 = odds[2].text.strip() if odds[2].text.strip() != "" else 0

            final_obj = {'live': True,
                         'date': "",
                         'league': league_name,
                         'home': team1,
                         'away': team2,
                         'score': "",
                         'minute': minute,
                         'b_1': float(b_1),
                         'b_x': float(b_x),
                         'b_2': float(b_2)
                         }

            matches.append(final_obj)

        except:
            continue

    return matches


# Function for getting the most recent league ids (using Selenium) and parsing them to blocks of twenty
def get_urls():
    driver = selenium_init()
    urls = set()
    driver.get("https://www.betano.cz/sport/fotbal/")
    links = driver.find_elements(By.CSS_SELECTOR,
                                 ".sport-block__region__body .sb-checkbox__link")
    url = "https://www.betano.cz/sport/fotbal/liga/"
    count = 0
    for link in links:
        if count == 20:
            urls.add(url[:-1])
            url = "https://www.betano.cz/sport/fotbal/liga/"
            count = 0

        postfix = link.get_attribute("href").split("/")[-2]
        if "o" in postfix:
            continue
        url += postfix + ","
        count += 1

    return urls
