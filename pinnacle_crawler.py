from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


# Helper function
def get_markets(market_btns, index):
    return market_btns[index].find(
        "span").get_text().strip() if "style_disabled" not in \
                                      " ".join(market_btns[index][
                                                   "class"]) else 0


def selenium_init():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("window-size=1920,1080")
    driver = webdriver.Chrome(options=chrome_options)
    driver.get(
        "https://www.pinnacle.com/cs/soccer/matchups/live")

    # Explicit wait for the matches to show, since it takes a while
    WebDriverWait(driver, timeout=10).until(
        EC.presence_of_element_located(
            (By.XPATH, "//div[contains(@class, 'style_gameInfo')]")))

    return driver


# Getting live matches
def live(driver):
    matches = []

    # Passing driver page source to external parsing tool in order to improve scraping speed
    soup = BeautifulSoup(driver.page_source, 'lxml')
    blocks = soup.select("div[class*='style_row_']")
    league = ""
    for block in blocks:
        league_elem = block.find("span", class_="ellipsis").get_text()
        # Filtering out unwanted elements
        if "Match" not in league_elem and "Zápas" not in league_elem:
            league = league_elem
            continue
        teams = block.select(
            "span[class*='event-row-participant']")  # Using CSS selector, since it is needed to locate an element by its partial class name
        team1 = teams[0].get_text().strip().replace(" (Match)", "")
        team2 = teams[1].get_text().strip().replace(" (Match)", "")
        minute = block.select("span[class*='style_live_']")[
            0].get_text().strip().replace("První poločas", "").replace(
            "Second Half", "").replace("Druhý poločas", "").replace(" – ",
                                                                    "").replace(
            " - ", "")
        scores = block.select("span[class*='style_staticScore']")
        score = scores[0].get_text().strip() + "-" + scores[
            1].get_text().strip()

        market_group = block.select("div[class*='style_buttons_']")[0]
        market_btns = market_group.select("button[class*='market-btn']")
        odd_home = get_markets(market_btns, 0)
        odd_draw = get_markets(market_btns, 1)
        odd_away = get_markets(market_btns, 2)

        final_obj = {'live': True,
                     'date': "",
                     'league': league,
                     'home': team1,
                     'away': team2,
                     'score': score,
                     'minute': minute,
                     'b_1': float(odd_home),
                     'b_x': float(odd_draw),
                     'b_2': float(odd_away)
                     }
        matches.append(final_obj)

    return matches
