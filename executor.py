import concurrent.futures
import time
import sys
import selenium.common.exceptions
import tipsport_crawler
import fortuna_crawler
import betano_crawler
import pinnacle_crawler
import differ
import requests
from concurrent.futures import ThreadPoolExecutor

url = "http://localhost:3000"


# Function moves data to the PostgreSQL table
def to_table(data, table_name, live):
    # requests are made via the Requests library, speed of the request doesn't
    # matter much since the request itself is very fast in this case
    requests.delete(url + "/api/game/all",
                    json={"table": table_name, "live": live})
    requests.post(url + "/api/game/new",
                           json={"games": data,
                                 "table": table_name})

# Main function, executes all scraping functions simultaneously
def crawl(fn, table_name, live, driver):
    while True:
        try:
            if driver is not None:
                # function call with driver parameter, since some scrapers
                # need driver to run simulated browser
                data = fn(driver)
            else:
                data = fn()

            to_table(data, table_name, live)

        except:
            print("Something went wrong, continuing...")

        finally:
            # delay specified by user, default 10 seconds
            time.sleep(delay)

# Function that updates league names
def league_names():
    while True:
        names = differ.leagues_teamPercent()
        requests.post(url + '/api/diff/league', json={"diffs": names})

        time.sleep(30)  # Not necessary to update league names immediately


# Final matching of the scraped matches
def final_matching():
    while True:
        # First, delete all records of existing previous matches with old betting odds values
        requests.delete(url + "/api/game/all", json={"table": "matched"})

        match = differ.match()
        requests.post(url + '/api/game/newMatched', json={"games": match})

        time.sleep(2)


# Thread creation and handling
def main():
    ress = []
    # Threading is used in other files for scraping, here it can be seen that range of usability is wide.
    # It is necessary that scraping scripts run simultaneously, since speed is the main factor
    pool = ThreadPoolExecutor()

    for scraper in [(betano_crawler.prematch, "betano_match", False, None),
                    (betano_crawler.live, "betano_match", True, b_l),
                    (fortuna_crawler.prematch, "fortuna_match", False, None),
                    (fortuna_crawler.live, "fortuna_match", True, f_l),
                    (tipsport_crawler.prematch, "tipsport_match", False, None),
                    (tipsport_crawler.live, "tipsport_match", True, None),
                    (pinnacle_crawler.live, "pinnacle_match", True, p_l)]:
        ress.append(pool.submit(crawl, fn=scraper[0], table_name=scraper[1],
                                live=scraper[2], driver=scraper[3]))

    ress.append(pool.submit(league_names))
    ress.append(pool.submit(final_matching))

    for completed in concurrent.futures.as_completed(ress):
        [txt, duration] = completed.result()
        print(f"{txt} : time taken {duration}", flush=True)


if len(sys.argv) == 2:
    try:
        delay = int(sys.argv[1])
    except TypeError:
        print("Argument must be of type int.")
        exit(1)

else:
    delay = 10

f_l = fortuna_crawler.selenium_init()
b_l = betano_crawler.selenium_init()
try:
    p_l = pinnacle_crawler.selenium_init()
except selenium.common.exceptions.TimeoutException:
    # It can happen that finding Pinnacle matches fails (for example when there is zero live matches on Pinnacle website)
    print("Pinnacle live matches not found")

main()
