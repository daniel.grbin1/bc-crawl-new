import requests
import json
import urllib3
import datetime
from urllib3.exceptions import NewConnectionError
from dotenv import load_dotenv
import os

load_dotenv()

url_localhost = "http://localhost:3000"
user_url = url_localhost


# Getting pre-match matches
def prematch():
    matches = []

    response = http.request('GET',
                            'https://partners.tipsport.cz/rest/external/'
                            'offer/v1/matches?idSuperSport=16',
                            headers={"Content-Type": "application/json",
                                     "Authorization": "Bearer " + token,
                                     "Cookie": "JSESSIONID=" + uuid})
    result = json.loads(response.data)
    for match in result["matches"]:
        if not match["homeParticipant"] or not match[
            "visitingParticipant"]:
            continue

        # Operations made on each match in order to fit the universal object sent to database
        # No need to use any parser library, the call returns String in the format of a JSON object
        date_start_api = match["dateClosed"]
        date_start = datetime.datetime.fromtimestamp(
            float(date_start_api) / 1000.0).strftime(
            '%Y-%m-%dT%H:%M:%S.%f')[:-3] + "+00:00"

        league = match["nameCompetition"].replace("\'", "")
        team1 = match["homeParticipant"].replace("\'", "")
        team2 = match["visitingParticipant"].replace("\'", "")

        team1_win = 0
        team2_win = 0
        draw = 0

        for opp in match["mainEvent"]["opps"]:
            if opp["oppNumber"] == "1":
                team1_win = opp["odd"]
            elif opp["oppNumber"] == "0":
                draw = opp["odd"]
            elif opp["oppNumber"] == "2":
                team2_win = opp["odd"]

        match_dict = {'live': False,
                      'date': date_start,
                      'league': league,
                      'home': team1,
                      'away': team2,
                      'b_1': team1_win,
                      'b_x': draw,
                      'b_2': team2_win
                      }

        matches.append(match_dict)

    return matches


# Getting live matches
def live():
    response = http.request('GET',
                            'https://partners.tipsport.cz/rest/external/offer/v2/live/matches?idSuperSport=16',
                            headers={"Content-Type": "application/json",
                                     "Authorization": "Bearer " + token,
                                     "Cookie": "JSESSIONID=" + uuid})

    result = json.loads(response.data)
    live_matches = []
    for match in result["matches"]:
        if match['mainEvent'] is None:
            continue

        date_start = datetime.datetime.fromtimestamp(
            int(match["dateStart"]) / 1e3).strftime(
            '%Y-%m-%dT%H:%M:%S.%f')[:-3] + "+00:00"
        league = match["nameCompetition"]
        team1 = match["homeParticipant"]
        team2 = match["visitingParticipant"]
        minute = match["time"].replace("'", "")
        score = match["scoreOffer"]

        team1_win = None
        draw = None
        team2_win = None

        for odd in match["mainEvent"]["opps"]:
            if odd["type"] == "1":
                team1_win = odd["odd"]
            if odd["type"] == "x":
                draw = odd["odd"]
            if odd["type"] == "2":
                team2_win = odd["odd"]

        match_dict = {'live': True,
                      'date': date_start,
                      'league': league,
                      'home': team1,
                      'away': team2,
                      "score": score,
                      "minute": minute,
                      'b_1': team1_win,
                      'b_x': draw,
                      'b_2': team2_win
                      }

        live_matches.append(match_dict)

    return live_matches


def tipsport_establish_session():
    http = urllib3.PoolManager(cert_reqs='CERT_NONE')
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    url = 'https://partners.tipsport.cz/rest/external/common/v2/session'
    myobj = {"username": os.getenv("TIPSPORT_USERNAME"),
             'password': os.getenv("TIPSPORT_PASSWORD")
             }
    # Example of sending Requests call with additional parameters (json, headers)
    y = requests.post(url, json=myobj,
                      headers={"Content-Type": "application/json"})
    token = json.loads(y.text)['sessionToken']
    uuid = y.cookies.get_dict()["JSESSIONID"]
    return token, uuid, http


token, uuid, http = tipsport_establish_session()
