import urllib3
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time

url_localhost = "http://localhost:3000"
user_url = url_localhost

http = urllib3.PoolManager(cert_reqs='CERT_NONE')
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# Selenium initialization
def selenium_init():
    chrome_options = Options()
    # Set browser to headless mode, so user doesn't get bothered with browser windows
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("window-size=1920,1080")

    driver = webdriver.Chrome(options=chrome_options)
    driver.get(
        "https://live.ifortuna.cz/sport/LCZFOOTBALL")

    # Example of wait - it takes some time for page to load and after that the cookie pop-up loads.
    # It is necessary to wait certain amount of time, otherwise the script would try to perform an action
    # before the element is available on the page
    WebDriverWait(driver, timeout=10).until(
        EC.element_to_be_clickable(
            (By.ID, "cookie-consent-button-accept")))

    # Since there is only a few locators needed, Selenium locating is used
    # and the speed of the scraping is not majorly affected
    driver.find_element(By.ID,
                        "cookie-consent-button-accept").click()

    time.sleep(5)
    arrows = driver.find_elements(By.CLASS_NAME,
                                  "live_league_header--collapsed")
    count = 0
    # Opening closed leagues tabs
    for arrow in arrows:
        count += 1
        arrow.click()
        # if count > 5:
        #    break

    return driver


def fetch(url):
    page_cr = http.request('GET',
                           "https://www.ifortuna.cz" + url)
    # Content parsed by BeautifulSoup library is returned
    return BeautifulSoup(page_cr.data, 'lxml')


def prematch():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("window-size=1920,1080")

    driver = webdriver.Chrome(options=chrome_options)
    driver.get(
        "https://www.ifortuna.cz/sazeni/fotbal")

    loading_count = 0
    # Scrolling to the bottom of the page repeatedly in order to load all matches
    while True:
        if loading_count > 20:
            break
        driver.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);")

        loading = driver.find_element(By.ID, "sport-events-list-ajax-loading")
        # If the loading box is hidden, add 1 to loading count. When the loading count is big enough,
        # no new matches are being loaded, and it is not necessary to scroll further on the webpage
        if loading.get_attribute("class") == "message-box css-hidden":
            loading_count += 1
        else:
            loading_count = 0

    # Passing the driver.page_source to external parsing tool, bs4 library with lxml parser (in order to improve speed)
    soup = BeautifulSoup(driver.page_source, "lxml")
    leagues = soup.find_all("section",
                            class_="competition-box")  # Locating an element by its class name
    matches = []
    for league in leagues:
        league_name = league["data-competition-name"]
        # Cutting off the cases which are not needed
        if "celkový" in league_name or \
                "celkové" in league_name or \
                "nejlepší" in league_name or \
                "branek" in league_name or \
                "umístění" in league_name or \
                "kdo lépe" in league_name:
            continue

        for match in league.find_all("tr", class_="tablesorter-hasChildRow"):
            teams = match.find("td", class_="col-title")["data-value"].split(
                ' - ')
            if len(teams) != 2:
                continue

            team1 = teams[0]
            team2 = teams[1]
            date_elem = match.find("span", class_="event-datetime")
            start_date = "" if date_elem is None else date_elem.get_text()
            odds = match.find_all("td", class_="col-odds")
            home_elem = odds[0].find(
                "a")  # Locating an element by its tag, since it is sufficient in this case
            odd_home = 0 if home_elem["disabled"] == "true" else home_elem[
                "data-value"]
            draw_elem = odds[1].find("a")
            odd_draw = 0 if draw_elem["disabled"] == "true" else draw_elem[
                "data-value"]
            away_elem = odds[2].find("a")
            odd_away = 0 if away_elem["disabled"] == "true" else away_elem[
                "data-value"]

            match_final = {'live': False,
                           'date': start_date,
                           'league': league_name,
                           'home': team1,
                           'away': team2,
                           'b_1': float(odd_home),
                           'b_x': float(odd_draw),
                           'b_2': float(odd_away)
                           }

            matches.append(match_final)

    return matches


def live(driver):
    matches_done = []

    soup = BeautifulSoup(driver.page_source, 'lxml')
    leagues = soup.find_all("div", class_="league")
    for league in leagues:
        league_name = league.find("div",
                                  class_="live_league_header__league_name_inner").get_text().strip()

        matches = league.find_all("div", class_="live-match")
        for match in matches:
            team1 = match.find("div",
                               class_="live-match-info__team--1").get_text().strip()
            team2 = match.find("div",
                               class_="live-match-info__team--2").get_text().strip()
            score = match.find("div",
                               class_="mini_scoreboard__teams").div.get_text().strip().replace(
                " ", "")
            minute = match.find("div",
                                class_="mini_scoreboard__game_time").span.get_text().strip()
            # Using CSS locator, because it is needed to find a subclass.
            # It would also be possible to perform this action in two steps
            # (first find one class, then in this class look for the other class)
            markets = match.select(
                ".live-match__top-market--1 .odds_button__value")

            # It can happen that the locators get also unwanted results (in this case no market area),
            # it is necessary to cut them off, otherwise wrong information get in the result object or the script fails
            if len(markets) == 0:
                continue

            b_1 = markets[0].get_text().strip() if "icon" not in markets[
                0]["class"] else "0"
            b_x = markets[1].get_text().strip() if "icon" not in markets[
                1]["class"] else "0"
            b_2 = markets[2].get_text().strip() if "icon" not in markets[
                2]["class"] else "0"

            final_obj = {'live': True,
                         'date': "",
                         'league': league_name,
                         'home': team1,
                         'away': team2,
                         'score': score,
                         'minute': minute,
                         'b_1': float(b_1),
                         'b_x': float(b_x),
                         'b_2': float(b_2)
                         }

            matches_done.append(final_obj)

    return matches_done
