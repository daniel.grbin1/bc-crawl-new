import requests
import json
import Levenshtein

url_localhost = "http://localhost:3000"
user_url = url_localhost


# Helper function
def get_matched(tipsport_league, matches):
    league_matched = ""
    ratio_final = 0

    for match in matches:
        other_league = match["league"]
        ratio = Levenshtein.ratio(tipsport_league.lower().replace("liga", ""),
                                  other_league.lower().replace("liga", "")
                                  .replace("league", "")
                                  .replace("superliga", ""))
        if ratio > ratio_final:
            ratio_final = ratio
            league_matched = other_league

    return None if ratio_final < 0.5 else league_matched


# Function with old approach, not used
def leagues():
    requests.delete(user_url + "/api/diff/league")

    req = requests.get(user_url + "/api/diff/league")
    obj = json.loads(req.text)
    diffs_league = obj["data"]["diffs"]
    added_t = set()

    for record in diffs_league:
        added_t.add(record["tipsport"])

    req = requests.get(user_url + "/api/game/all?site=tipsport")
    obj = json.loads(req.text)
    games_tipsport = obj["data"]["games"]

    req = requests.get(user_url + "/api/game/live?site=fortuna")
    obj = json.loads(req.text)
    games_fortuna_live = obj["data"]["games"]

    req = requests.get(user_url + "/api/game/prematch?site=fortuna")
    obj = json.loads(req.text)
    games_fortuna_prematch = obj["data"]["games"]

    req = requests.get(user_url + "/api/game/all?site=betano")
    obj = json.loads(req.text)
    games_betano = obj["data"]["games"]

    matched = []
    cnt = 0
    cnt_succ = 0
    for game in games_tipsport:
        league = game["league"]
        if league in added_t:
            continue

        added_t.add(league)

        league_fortuna_final_live = get_matched(league, games_fortuna_live)
        league_fortuna_final_prematch = get_matched(league,
                                                    games_fortuna_prematch)
        league_betano_final = get_matched(league, games_betano)

        if league_fortuna_final_live is not None or \
                league_fortuna_final_prematch is not None or \
                league_betano_final is not None:

            matched.append({"tipsport": league,
                            "fortuna_live": league_fortuna_final_live,
                            "fortuna_prematch": league_fortuna_final_prematch,
                            "betano": league_betano_final,
                            "fixed": league_fortuna_final_live is not None and \
                                     league_fortuna_final_prematch is not None and \
                                     league_betano_final is not None})
            cnt_succ += 1
        else:
            cnt += 1

    return matched


# Helper functions
def get_games_obj(site, type):
    req = requests.get(user_url + "/api/game/" + type + "?site=" + site)
    obj = json.loads(req.text)
    return obj["data"]["games"]


def fill_dicts(games, leagues):
    for game in games:
        if game["league"] not in leagues:
            leagues[game["league"]] = []

        leagues[game["league"]].append(game["home"])
        leagues[game["league"]].append(game["away"])


# Function with old approach, not used
def leagues_extra():
    requests.delete(user_url + "/api/diff/league")

    req = requests.get(user_url + "/api/diff/league")
    obj = json.loads(req.text)
    diffs_league = obj["data"]["diffs"]
    added_t = set()
    for record in diffs_league:
        added_t.add(record["tipsport"])

    games_fortuna_live = get_games_obj("fortuna", "live")
    games_fortuna_prematch = get_games_obj("fortuna", "prematch")
    games_tipsport = get_games_obj("tipsport", "all")
    games_betano = get_games_obj("betano", "all")
    games_pinnacle = get_games_obj("pinnacle", "all")

    leagues_b = {}
    leagues_t = {}
    leagues_fl = {}
    leagues_fp = {}
    leagues_pn = {}

    fill_dicts(games_betano, leagues_b)
    fill_dicts(games_fortuna_live, leagues_fl)
    fill_dicts(games_fortuna_prematch, leagues_fp)
    fill_dicts(games_tipsport, leagues_t)
    fill_dicts(games_pinnacle, leagues_pn)

    matched = []

    for key, value in leagues_t.items():
        if key in added_t:
            continue

        joined = "".join(value)

        final_betano = ""
        final_fortuna = ""
        final_fortuna_live = ""
        final_pinnacle = ""
        for league in [leagues_b, leagues_fp, leagues_fl, leagues_pn]:
            ratio = 0
            final = ""

            for k, v in league.items():
                jb = "".join(v)
                rat = Levenshtein.ratio(joined, jb)
                if rat > ratio:
                    ratio = rat
                    final = k
                    finanal = jb

            if ratio > 0.5:
                if league == leagues_b:
                    final_betano = final
                elif league == leagues_fp:
                    final_fortuna = final
                elif league == leagues_fl:
                    final_fortuna_live = final
                elif league == leagues_pn:
                    final_pinnacle = final

        matched.append({"tipsport": key,
                        "fortuna_live": final_fortuna_live,
                        "fortuna_prematch": final_fortuna,
                        "betano": final_betano,
                        "pinnacle": final_pinnacle,
                        "fixed": False})

    return matched


# Main function that matches league names by checking the syntax of the team names
def leagues_teamPercent():
    requests.delete(user_url + "/api/diff/league")

    req = requests.get(user_url + "/api/diff/league")
    obj = json.loads(req.text)
    diffs_league = obj["data"]["diffs"]
    added_t = set()
    for record in diffs_league:
        added_t.add(record["tipsport"])

    games_fortuna_live = get_games_obj("fortuna", "live")
    games_fortuna_prematch = get_games_obj("fortuna", "prematch")
    games_tipsport = get_games_obj("tipsport", "all")
    games_betano = get_games_obj("betano", "all")
    games_pinnacle = get_games_obj("pinnacle", "all")

    leagues_b = {}
    leagues_t = {}
    leagues_fl = {}
    leagues_fp = {}
    leagues_pn = {}

    fill_dicts(games_betano, leagues_b)
    fill_dicts(games_fortuna_live, leagues_fl)
    fill_dicts(games_fortuna_prematch, leagues_fp)
    fill_dicts(games_tipsport, leagues_t)
    fill_dicts(games_pinnacle, leagues_pn)

    matched = []

    for key, value in leagues_t.items():
        if key in added_t:
            continue

        final_betano = ""
        final_fortuna = ""
        final_fortuna_live = ""
        final_pinnacle = ""

        for league in [leagues_b, leagues_fp, leagues_fl, leagues_pn]:
            ratio = 0
            final = ""

            for k, v in league.items():
                teams_less = v if len(v) < len(value) else value
                teams_more = v if teams_less != v else value

                final_ratio = 0

                for team1 in teams_less:
                    ratio_teams = 0
                    for team2 in teams_more:
                        # Levenshtein library is used to find the similarity rate in teams names
                        rat = Levenshtein.ratio(team1, team2)
                        if rat > ratio:
                            ratio_teams = rat

                    final_ratio += ratio_teams

                final_ratio /= len(teams_less)
                if final_ratio > ratio:
                    ratio = final_ratio
                    final = k

            # if the similarity rate is above the value of 0.55, most of the
            # time the teams are the same and the matching is correct
            if ratio > 0.55:
                if league == leagues_b:
                    final_betano = final
                elif league == leagues_fp:
                    final_fortuna = final
                elif league == leagues_fl:
                    final_fortuna_live = final
                elif league == leagues_pn:
                    final_pinnacle = final

        matched.append({"tipsport": key,
                        "fortuna_live": final_fortuna_live,
                        "fortuna_prematch": final_fortuna,
                        "betano": final_betano,
                        "pinnacle": final_pinnacle,
                        "fixed": False})

    return matched


# Helper function
def team_diff(game_tipsport, games_other, league_obj, abbr, match):
    for game_other in games_other:
        if game_other["league"] == league_obj:
            ratio_home = Levenshtein.ratio(game_other["home"],
                                           game_tipsport["home"])
            ratio_away = Levenshtein.ratio(game_other["away"],
                                           game_tipsport["away"])

            if ratio_home > 0.5 and ratio_away > 0.5:
                match[abbr + "_1"] = game_other["b_1"]
                match[abbr + "_x"] = game_other["b_x"]
                match[abbr + "_2"] = game_other["b_2"]


# Final matching, uses syntax checking with the Levenshtein library
def match():
    req = requests.get(user_url + "/api/diff/league")
    obj = json.loads(req.text)
    diffs_league = obj["data"]["diffs"]

    games_fortuna = get_games_obj("fortuna", "all")
    games_tipsport = get_games_obj("tipsport", "all")
    games_betano = get_games_obj("betano", "all")
    games_pinnacle = get_games_obj("pinnacle", "all")

    matched = []
    cnt = 0
    cnt_success = 0
    cnt_bofa = 0
    for game in games_tipsport:
        match = game
        del match["id"]
        league_obj = None

        for lg in diffs_league:
            if lg["tipsport"] == game["league"]:
                league_obj = lg

        if league_obj is None:
            cnt += 1
            continue

        fortuna_type = league_obj["fortuna_live"] if game["live"] else \
            league_obj[
                "fortuna_prematch"]

        team_diff(game, games_fortuna, fortuna_type, "fa", match)
        team_diff(game, games_betano, league_obj["betano"], "bo", match)
        team_diff(game, games_pinnacle, league_obj["pinnacle"], "pn", match)

        if "fa_1" in match or "bo_1" in match or "pn_1" in match:
            if "fa_1" in match and "bo_1" in match:
                cnt_bofa += 1

            matched.append(match)
            cnt_success += 1
        else:
            cnt += 1

    print("success: " + str(cnt_success))
    print("failed: " + str(cnt))
    # print("succes FA and BO: " + str(cnt_bofa))
    # print("matches: " + str(len(games_tipsport)))

    return matched
